import logo from './logo.svg';
import './App.css';

import {Box, AppBar, Toolbar, Typography,card} from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import{useEffect,useState} from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';

import YouTube from 'react-youtube';


function App() {

  const [temperature, setTemperaturfe] = useState();

  
  const weatherAPIBaseUrL= 
  "https://api.openweathermap.org/data/2.5/weather?";
  const city = "SongKhla";
  const apiKey = "64780883041dafaed05819af135e5627";

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '64780883041dafaed05819af135e5627',
    lat: '48.137154',
    lon: '11.576124',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });


  // OPenwheatermap ApiKey : 64780883041dafaed05819af135e5627

  useEffect( () => {
      setTemperaturfe("----");

      axios.get(weatherAPIBaseUrL+"q="+city+"&appid="+apiKey).then( (response)=>{
        let data = response.data;
        let temp = data.main.temp-273;
        setTemperaturfe(temp.toFixed(2))
      })
  }
  
  , [] );

  return (
   <Box sx= {{ flexGrow : 1, width: "100%" }} > 
        <AppBar position="static">
                <Toolbar>
                  <Typography variant="h6">
                      WeatherApp
                  </Typography>
                </Toolbar>
        </AppBar>

        <Box sx={{ justifyContent : 'center',margin : "20px", width : "100%", display : "flex"}}>
                   <Typography variant='h4'>
                      อากาศหาดใหญ่วันนี้
                  </Typography>
        </Box>

        <Box sx={{ justifyContent : 'center',margin : "20px", width : "100%", display : "flex"}}>
          <Card sx={{ Width: 275  }}>
                        <CardContent>
                             <Typography sx={{ fontSize: 14 }} color="text.secondary">
                                อุณหภูมิหาดใหญ่วันนี้คือ
                              </Typography>
                              <Typography variant="h5" component="div">
                                   { temperature }
                              </Typography>
                              <Typography variant="h6" component="div">
                                   องศาเซลเซียส
                              </Typography>
                          </CardContent>          
             </Card>
             <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="en"
      locationLabel="Munich"
      unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
      showForecast
    />
        </Box>
        <YouTube videoId="8mqM-a03eu0"/>
    </Box>
  );  
}

export default App;

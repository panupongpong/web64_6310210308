import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Middle from './components/Middle';

function App() {
  return (
    <div className="App">
      <Header/> 
      <Body />
      <Middle/>
      <Footer/>
    </div>
  );
}

export default App;

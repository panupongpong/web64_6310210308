
import './Body.css'

function Body () {
    return(
        <div id='song' align = "center">
           
            <h2>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/zRToTk5b620" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </h2>
            <div id='stry' align = "center">
                <h2> Synopsis </h2>
                
                <p id='a'> เด็กหนุ่มตัวเอกของเรื่องนี้   เป็นพวกฮิคิโคโมริ (พวกที่ชอบเก็บตัวอยู่กับบ้าน) </p>
                <p id='a'> ที่จู่ๆ ก็พบว่าตัวเองมาอยู่ในโลกแฟนตาซีที่มีประเทศชื่อ “ลูกานิก้า”</p>
                <p id='a'> จนกระทั่งได้พบกับสาวน้อยคนหนึ่งที่ชื่อ “เอมิเลีย”  ซึ่งนั่นคือจุดเริ่มต้นที่ตัวเขา </p>
                <p id='a'> ต้องไปเจอกับความตายและการเริ่มต้น  ณ  จุดใดจุดหนึ่ง</p>
                <p id='a'> อันเป็นความทรงจำครั้งสุดท้ายในโลกที่ไม่รู้จักซ้ำแล้วซ้ำเล่า  </p>
                <p id='a'> เพื่อยุติภาพความตายที่วนลูปไม่สิ้นสุด  ไม่ว่าจะของตัวเอง  หรือเอมิเลีย</p>
                <p id='a'> หรือคนอื่นที่มาเกี่ยวข้องกันในจุดบรรจบของเหตุการณ์อันไม่ควรเกิดขึ้น </p> 
                <p id='a'> เขาจึงต้องพยายามรักษาชีวิตของตัวเองให้มุ่งไปสู่เหตุการณ์ที่ควรจะเป็นที่สุด</p>
                
            </div>
            <hr />
            <hr />
        </div>
    );
}

export default Body;
const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi',(req, res) =>{
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}
    if( !isNaN(weight) && !isNaN(height)){
        let bmi = weight / (height * height)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }else {

        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))

})

app.get('/triangle',(req, res) =>{
     let base = parseFloat(req.query.base)
     let high = parseFloat(req.query.high )
   
    var result = {}
    if( !isNaN(high) && !isNaN(base)){
        let triangle = 0.5 * base * high
        result = {
            "status" : 200,
            "triangle" : triangle
        }
    }else {

        result = {
            "status" : 400,
            "message" : "High or Base is not a number"
        }
    }
    res.send(JSON.stringify(result))

})

app.post('/score',(req, res) =>{
    let name = ''
    let score = parseFloat(req.query.score)
    var result = {}
    if(!isNaN(score)){
        if(score>100)
        {
            score = "error"
        }
        if(score <= 100 && score >=80)
        {
            score = "A"
        }else if(score >=70)
        {
            score = "B"
        }else if(score >=60)
        {
            score = "C"
        }else if(score >=50)
        {
            score = "D"
        }else if(score <50 && score >=0)
        {
            score = "E"
        }else 
        {
            score = "error"
        }
        
        result = {
            "status" : 200,
            "score" : score
        }

    }else {

        result = {
            "status" : 400,
            "message" : "Score is not a number"
        }
    }
    res.send(JSON.stringify(result))

})

app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

function AboutUs (props) {

    return(
        <div>
            <h2> จัดทำโดย: {props.name} </h2>
            <h3> ติดต่อที่ {props.address} </h3>
            <h3> กลุ่มอยู่ที่ {props.provience}</h3>
        </div>
    );
}

export default AboutUs;
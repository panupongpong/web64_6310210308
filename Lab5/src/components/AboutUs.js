import  Box  from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { width } from '@mui/system';

function AboutUs (props) {

    return(
        <Box sx={{ width: "60%" }}>
        <Paper elevation={3}>
            <h2> จัดทำโดย: {props.name} </h2>
            <h3> ติดต่อที่ {props.address} </h3>
            <h3> กลุ่มอยู่ที่ {props.provience}</h3>
        </Paper>
        </Box>
    );
}

export default AboutUs;
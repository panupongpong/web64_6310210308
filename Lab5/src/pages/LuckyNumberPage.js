
import { useEffect, useState } from "react";
import LuckyNumber from "../components/LuckyNumber";

function LuckyNumberPage (){

    const[numlucky,setNumLucky] = useState("");
    const[luckyresult,setLuckyResult] = useState("");
    const[randomnum,setRandomNum] = useState("");
    
    useEffect(()=>{
        setRandomNum(Math.round((Math.random()*99)+0));
    });


    function Calluck(){
        let l = parseInt(numlucky)
        let rd = parseInt(randomnum)

        if (l == "69" || l == rd){
            setLuckyResult("ถูกแล้วจ้าา")
        }
        else {
            setLuckyResult("ผิด")
        }
    }

   
    return(
        <div align = "left">
            <div align = "center">
            ยินดีต้อนรับสู่การเสี่ยงทาย 
                <hr/>

                กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <input type = "text"
                              value={numlucky}
                              onChange={ (e) => { setNumLucky(e.target.value) } } /> <br />
            <button onClick={ ()=>{ Calluck () } }> ทาย </button>

        { numlucky != 0 &&

            <div>
            <LuckyNumber
            num = {luckyresult}
            />
            </div>

         }
            </div>
        </div>

    );
}
export default LuckyNumberPage;

